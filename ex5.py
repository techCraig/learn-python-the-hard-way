name = 'Zed A. Shaw'
age = 35 # not a lie
height = 74 # inches
weight = 180 # lbs
eyes = 'Blue'
teeth = 'White'
hair = 'Brown' 


print ("Let's talk about %r." % name)
print ("He's %d inches tall." % height)
print ("He's %r pounds heavy." % weight)
print ("Actually that's not too heavy")
print ("He's got %s eyes and %s hair." % (eyes, hair))
print ("His teeth are usually %s depending on the coffee." % teeth)


print ("If I add %d, %d, and %d I get %d." % (age, height, weight, age + height + weight))



# Format characters list
# %s --> Strings
# %d --> Integers
# %f --> Floating point numbers

lbs = 172
kilo_formula = 2.2046

conversion = lbs / kilo_formula

print (conversion)
